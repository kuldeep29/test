package main

import (
	"fmt"
	"tcstest/pkg/employee"
	"tcstest/pkg/rest"
)

func main() {
	fmt.Println("Welcome to employee web app")

	es := employee.NewEmployeeService()
	app := rest.NewAppContext(es)

	app.SetupRoutes()
	app.Run()
}
