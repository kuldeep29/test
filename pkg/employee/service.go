package employee

import (
	"errors"
	"fmt"
	"tcstest/pkg/message"
)

type Servicer interface {
	GetEmployees() ([]message.Employee, error)
	GetEmployeeById(string) (*message.Employee, error)
}

func NewEmployeeService() *Service {
	var s Service
	return &s
}

type Service struct {
}

func (s Service) GetEmployees() ([]message.Employee, error) {
	var employees []message.Employee
	employees = listEmpoyees("")

	return employees, nil
}

func (s Service) GetEmployeeById(id string) (*message.Employee, error) {
	employees := listEmpoyees(id)
	if employees == nil {
		return nil, errors.New("Not found employee")
	}

	return &employees[0], nil
}

func listEmpoyees(id string) []message.Employee {
	employee1 := message.Employee{
		ID:   "123",
		Name: "kuldeep",
	}
	employee2 := message.Employee{
		ID:   "124",
		Name: "foo",
	}
	employees := []message.Employee{employee1, employee2}

	if id != "" {
		singleempoye := []message.Employee{}
		for _, employee := range employees {
			fmt.Println(id, employee.ID)
			if id == employee.ID {
				singleempoye = append(singleempoye, employee)
				return singleempoye
			}
		}

		return nil
	}

	return employees
}
