package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (app *AppContext) getEmployesHandler(c *gin.Context) {
	employees, err := app.EmployeeService.GetEmployees()
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
	}

	c.IndentedJSON(http.StatusOK, employees)
}

func (app *AppContext) getEmployeIDHandler(c *gin.Context) {
	id := c.Param("id")
	employees, err := app.EmployeeService.GetEmployeeById(id)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
	}

	c.IndentedJSON(http.StatusOK, employees)
}
