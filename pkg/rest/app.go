package rest

import (
	"github.com/gin-gonic/gin"
	"tcstest/pkg/employee"
)

type AppContext struct {
	router          *gin.Engine
	EmployeeService employee.Servicer
}

func NewAppContext(es employee.Servicer) *AppContext {
	app := &AppContext{
		EmployeeService: es,
	}

	return app
}

func (app *AppContext) Run() {
	app.router.Run(":8081")
}

func (app *AppContext) SetupRoutes() {
	app.router = gin.Default()

	app.router.GET("/v1/employess", app.getEmployesHandler)
	app.router.GET("/v1/employess/:id", app.getEmployeIDHandler)
	app.router.Run(":8081")
}
